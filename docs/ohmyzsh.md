# Oh My Zsh
> https://ohmyz.sh/
> Oh My Zsh is a delightful, open source, community-driven framework for managing your Zsh configuration. It comes bundled with thousands of helpful functions, helpers, plugins, themes, and a few things that make you shout...

`Oh My Zsh` feature will:
- Install `Oh My ZSH` for current user.

`Copy` feature will:
- Copy `.zshrc` to user home.
    - It Enables `Oh My Zsh`.
    - It configures `Oh My Zsh`
    - See [copy.md](./copy.md) for more details.
