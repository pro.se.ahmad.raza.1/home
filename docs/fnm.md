# FNM - Fast Node Version Manager
> [Fast Node Version Manager](https://github.com/Schniz/fnm): 🚀 Fast and simple Node.js version manager, built in Rust 

`FNM` feature will:
- Install FNM
- Install `NodeJS` - latest LTS version
    - Set it as a default version

`Copy Files` feature will:
- Copy `.zshrc` to user HOME directory.
    - `.zshrc` includes necessary environment variables, and `FNM` enablement.

### Requirements
- `curl` or `wget`