# Summary

- [Home](./index.md)
- [Feature / Oh My Zsh](./ohmyzsh.md)
- [Feature / FNM](./fnm.md)
- [Feature / Node Modules](./node-modules.md)
- [Feature / NvChad](./nvchad.md)
- [Feature / TPM](./tpm.md)
- [Feature / Copy](./copy.md)
- [Feature / Configure](./configure.md)
