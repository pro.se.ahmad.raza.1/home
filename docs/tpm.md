# TPM - Tmux Plugin Manager
> [Tmux Plugin Manager](https://github.com/tmux-plugins/tpm): Installs and loads tmux plugins.

`TPM` feature will:
- Install TPM

`Copy Files` feature will:
- Copy directory `.tmux.conf` to user HOME directory.
    - `.config/tmux/` loads and configures TPM
    - See [copy.md](./copy.md) for more details. 