# Node Modules

`Node Modules` feature will install some sensible global node modules. These modules will be installed for the installed `node` and `npm`.

Installed Modules are:

| Module | Info |
| ------ | ---- |
[nodemon](https://www.npmjs.com/package/nodemon) | `Monitor for any changes in your node.js application and automatically restart the server - perfect for development`
[pm2](https://www.npmjs.com/package/pm2) | `PM2 is a production process manager for Node.js applications with a built-in load balancer. It allows you to keep applications alive forever, to reload them without downtime and to facilitate common system admin tasks.`
[serve](https://www.npmjs.com/package/serve) | `serve helps you serve a static site, single page application or just a static file (no matter if on your device or on the local network). It also provides a neat interface for listing the directory's contents:`
[@angular/cli](https://www.npmjs.com/package/@angular/cli) | `Angular CLI - The CLI tool for Angular.` Read more about [Angular](https://angular.dev).
[@ionic/cli](https://www.npmjs.com/package/@ionic/cli) | `The Ionic command line interface (CLI) is your go-to tool for developing Ionic apps.`. Read more about [Ionic](https://ionic.io/).
[react-scripts](https://www.npmjs.com/package/react-scripts) | `This package includes scripts and configuration used by Create React App.`. Read more about [React.js](https://react.dev) and [Create React App](https://create-react-app.dev).
[sequelize-cli](https://www.npmjs.com/package/sequelize-cli) | `The Sequelize Command Line Interface (CLI)`. Read more about [Sequelize](https://sequelize.org/) and [Sequelize CLI Tool](https://github.com/sequelize/cli).